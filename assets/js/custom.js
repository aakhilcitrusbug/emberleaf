$('.carousel').carousel({
  interval: 5000
})

$(document).ready(function(){
 
  wow = new WOW({
    boxClass: 'wow', // default
    animateClass: 'animated', // default
    offset: 0, // default
    mobile: false, // default
    live: true, // default
  });
  wow.init();
 
 });
 
/* Set navigation */

function openNav() {
  $("#mySidenav").addClass("width80");
  $("#nav-res").addClass("opacityon");
  $(".cd-shadow-layer").addClass("displayblock");

  // document.getElementById("mySidenav").style.width = "80%";
  // document.getElementById("nav-res").style.opacity = "1";
  // document.getElementById("cd-shadow-layer").style.display = "block";
  
}

function closeNav() {
  // document.getElementById("mySidenav").style.width = "0";
  // document.getElementById("nav-res").style.opacity = "0";
  // document.getElementById("cd-shadow-layer").style.display = "none";  

  $("#mySidenav").removeClass("width80");
  $("#nav-res").removeClass("opacityon");
  $(".cd-shadow-layer").removeClass("displayblock");
} 

function openSearch() {
  document.getElementById("myOverlay").style.display = "block";
}

function closeSearch() {
  document.getElementById("myOverlay").style.display = "none";
}


$(document).ready(function(){ 
  $(".cd-shadow-layer").click(function(){
    closeNav(); 
  });

  /*$(".hover-megamenu").hover(function(){
    $(".megamenu-submenu").removeClass("hidden");
  });*/

  $(".hover-megamenu, .megamenu-submenu").hover(function(){
    $(".megamenu-submenu").removeClass("hidden");
    }, function(){
    $(".megamenu-submenu").addClass("hidden");
  });


  


});

/* end of navigation */

$(".sb-icon-search").click(function(){
  $(".navbar-nav").addClass("sb-search-active");
  $(".sb-search").addClass("sb-search-open");
});


$(".sb-search-close ").click(function(){
  $(".navbar-nav").removeClass("sb-search-active");
  $(".sb-search").removeClass("sb-search-open");
});



$('.owl-gallery').owlCarousel({
  loop:true,
  margin: 10,
  nav:true,
  navText: ['<span class="span-roundcircle left-roundcircle"><img src="images/icon/left-arrow-gallery.svg" class="left_arrow_icon" alt="arrow" /></span>','<span class="span-roundcircle right-roundcircle"><img src="images/icon/right-arrow-gallery.svg" class="right_arrow_icon" alt="arrow" /></span>'],
  dots: false,
  // animateIn: 'fadeIn',
  // animateOut: 'fadeOut',
  smartSpeed:2000,
  responsive:{
      0:{
          items:1
      },
      600:{
          items:1
      },
      1000:{
          items:1
      }
  }
})



$('.owl-testimonial').owlCarousel({
  loop:true,
  margin: 10,
  nav:true,
  navText: ['<span class="span-roundcircle left-roundcircle"><img src="images/icon/left-arrow.svg" class="left_arrow_icon" alt="arrow" /></span>','<span class="span-roundcircle right-roundcircle"><img src="images/icon/right-arrow.svg" class="right_arrow_icon" alt="arrow" /></span>'],
  dots: false,
  // animateIn: 'fadeIn',
  // animateOut: 'fadeOut',
  smartSpeed:2000,
  responsive:{
      0:{
          items:1
      },
      600:{
          items:1
      },
      1000:{
          items:1
      }
  }
})



$('.owl-carousel-view').owlCarousel({
  loop:true,
  margin: 0,
  nav: true,
  navText: ['<span class="span-roundcircle left-roundcircle"><img src="assets/images/icons/left-arrow.svg" class="left_arrow_icon" alt="arrow" /></span>','<span class="span-roundcircle right-roundcircle"><img src="assets/images/icons/right-arrow.svg" class="right_arrow_icon" alt="arrow" /></span>'],
  dots: false,
  // animateIn: 'fadeIn',
  // animateOut: 'fadeOut',
  smartSpeed:2000,
  responsive:{
      0:{
          items:1 
      },
      600:{
          items:2
      },
      1000:{
          items:4
      }
  }
})


$('.owl-carousel').owlCarousel({
  loop:true,
  margin: 10,
  nav:false,
  navText: ['<span class="span-roundcircle left-roundcircle"><img src="images/icon/left-arrow.svg" class="left_arrow_icon" alt="arrow" /></span>','<span class="span-roundcircle right-roundcircle"><img src="images/icon/right-arrow.svg" class="right_arrow_icon" alt="arrow" /></span>'],
  dots: false,
  // animateIn: 'fadeIn',
  // animateOut: 'fadeOut',
  smartSpeed:2000,
  responsive:{
      0:{
          items:2 
      },
      600:{
          items:5
      },
      1000:{
          items:7
      },
      1400:{
        items:10
    }
  }
})






$(document).ready(function(){
	
 

  $("#toggle-read").click(function() {
    var elem = $("#toggle-read").text();
    if (elem == "Read More...") {
      $("#toggle-read").text("Read Less");
      $("#text_hide_show").show();
    } else {
      $("#toggle-read").text("Read More...");
      $("#text_hide_show").hide();
    }
  });

  // ===== Scroll to Top ==== 
  $(window).scroll(function() {
    if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
        $('#return-to-top').fadeIn(200);    // Fade in the arrow
    } else {
        $('#return-to-top').fadeOut(200);   // Else fade out the arrow
    }
  });
  $('#return-to-top').click(function() {      // When arrow is clicked
    $('body,html').animate({
        scrollTop : 0                       // Scroll to top of body
    }, 500);
  });

 
});


$(window).scroll(function(){
  var sticky = $('.header-div'),
      scroll = $(window).scrollTop();

  if (scroll >= 80) sticky.addClass('slim');
  else sticky.removeClass('slim');
});




// $(window).scroll(function(){
//   var sticky = $('.slide-bar-mobile'),
//       scroll = $(window).scrollTop();

//   if (scroll >= 100) sticky.addClass('fixed');
//   else sticky.removeClass('fixed');
// });


(function($) {
  var $window = $(window),
      $html = $('.sidebar');

  function resize() {
      if ($window.width() < 514) {
          return $html.addClass('slide-bar-mobile');
      }

      $html.removeClass('slide-bar-mobile');
  }

  $window
      .resize(resize)
      .trigger('resize');
})(jQuery);


$('a').click(function() {
  $('.btn-collapse').addClass("collapsed");
  $('.collapse').removeClass("show");
});


$(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body ').animate({
        scrollTop: $(hash).offset().top - 120
      }, 1000, function(){
        // Add hash (#) to URL when done scrolling (default click behavior)

        // window.location.hash = hash;
      });
    } // End if
  });
});


$(".cat-link").click(function(){
  $('.cat-link').removeClass("active");
  $(this).addClass("active");
});



$(document).ready(function(){
  $('.count').prop('disabled', true);
   $(document).on('click','.plus',function(){
  $('.count').val(parseInt($('.count').val()) + 1 );
  });
    $(document).on('click','.minus',function(){
    $('.count').val(parseInt($('.count').val()) - 1 );
      if ($('.count').val() == 0) {
      $('.count').val(1);
    }
      });
});